#!/usr/bin/env python3

import sys
import re


def help():
    print('USAGE: ./check_citations.py FILE_NAME ...')


def check_file(path):
    try:
        f = open(path, 'r')
    except Exception:
        print(f'{path}: cannot be opened')
        exit(2)

    citations = [int(c) for c in cit_re.findall(f.read())]
    citations.sort()

    missing = []

    prev = 0
    for c in citations:
        if c != (prev + 1):
            missing.extend([str(s) for s in range(prev + 1, c)])
        prev = c
    print(f'File: {path}')
    if missing:
        print('Missing: {}'.format(', '.join(missing)))
    else:
        print('Okay')


if __name__ == '__main__':
    if len(sys.argv) < 2:
        help()
        exit(1)

    cit_re = re.compile(r'\[c([0-9]+)\]')

    for path in sys.argv[1:]:
        check_file(path)
