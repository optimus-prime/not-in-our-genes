#+TITLE: The New Right and the Old Determinism

The start of the decade of the 1980s was symbolized, In Britain and the United
States, by the coming to power of new conservative governments; and the
conservatism of Margaret Thatcher and Ronald Reagan marks in many ways a
decisive break in the political consensus of liberal conservatism that has
characterized governments in both countries for the previous twenty years or
more. It represents the expression of a newly coherent and explicit conservative
ideology[fn:1] often described as the New Right.[c1]

New Right ideology has developed in Europe and North America in response to the
gathering social and economic crises of the past decade. Abroad, in Africa,
Asia, and Latin America, there have been struggles against nationalist forces
determined to throw off the yoke of political and economic exploitation and
colonialism. At home, there has been increasing unemployment, relative economic
decline, and the rise of new and turbulent social movements. During the sixties
and early seventies, Europe and North America experienced an upsurge of new
movements, some of which were quite revolutionary: struggles of shop-floor
workers against meritocratic ruling elites, blacks against white racism, women
against patriarchy, students against educational authoritarianism, welfare
clients against the welfare bureaucrats. The New Right criticizes the liberal
response to these challenges of the previous decades, the steady increase in
state intervention, and the growth of large institutions, resulting in
individuals losing control over their own lives, and hence an erosion of the
traditional values of self-reliance which the New Right regards as
characterizing the Victorian laissez-faire economy. This movement has been
strengthened, in the later seventies and eighties, by the fact that liberalism
has fallen into a self-confessed disarray, leaving the ideological battlefield
relatively open to the New Right.

The response of the liberal consensus to challenges to its institutions has
always been the same: an increase in interventive programs of social
amelioration: of projects in education, housing, and inner-city renewal. By
contrast, the New Right diagnoses the liberal medicine as merely adding to the
ills by progressively eroding the "natural" values that had characterized an
earlier phase of capitalist industrial society. In the words of the conservative
theoretician Robert Nisbet, it is a reaction against the present-day "erosion of
traditional authority in kinship, locality, culture, language, school, and other
elements of the social fabric."[c3]

But New Right ideology goes further than mere conservatism and makes a decisive
break with the concept of an organic society whose members have reciprocal
responsibilities. Underlying its /cri de coeur/ about the growth in state power
and the decline in authority—underlying even the monetarism of Milton
Friedman—is a philosophical tradition of individualism, with its emphasis on the
priority of the individual over the collective. That priority is seen as having
both a moral aspect, in which the rights of individuals have absolute priority
over the rights of the collectivity—as, for example, the right to destroy
forests by clear-cutting in order to maximize immediate profit—and an
ontological aspect, where the collectivity is nothing more than the sum of the
individuals that make it up. And the roots of this methodological individualism
lie in a view of human nature which it is the main purpose of this book to
challenge.

Philosophically this view of human nature is very old; it goes back to the
emergence of bourgeois society in the seventeenth century and to Hobbes's view
of human existence as a /bellum omnium contra omnes/, a war of all against all,
leading to a state of human relations manifesting competitiveness, mutual fear,
and the desire for glory. For Hobbes, it followed that the purpose of social
organization was merely to regulate these inevitable features of the human
condition.[c4] And Hobbes's view of the human condition derived from his
understanding of human biology; it was biological inevitability that made humans
what they were. Such a belief encapsulates the twin philosophical stances with
which this book is concerned, and to which, in the pages that follow, we will
return again and again.

The first is /reductionism/—the name given to a set of general methods and modes
of explanation both of the world of physical objects and of human societies.
Broadly, reductionists try to explain the properties of complex
wholes—molecules, say, or societies—in terms of the units of which those
molecules or societies are composed. They would argue, for example, that the
properties of a protein molecule could be uniquely determined and predicted in
terms of the properties of the electrons, protons, etc., of which its atoms are
composed. And they would also argue that the properties of a human society are
similarly no more than the sums of the individual behaviors and tendencies of
the individual humans of which that society is composed. Societies are
"aggressive" because the individuals who compose them are "aggressive," for
instance. In formal language, reductionism is the claim that the compositional
units of a whole are ontologically prior to the whole that the units comprise.
That is, the units and their properties exist before the whole, and there is a
chain of causation that runs from the units to the whole.[c5]

The second stance is related to the first; indeed. It is in some senses a
special case of reductionism. It is that of /biological determinism/. Biological
determinists ask, in essence, Why are individuals as they are? Why do they do
what they do? And they answer that human lives and actions are inevitable
consequences of the biochemical properties of the cells that make up the
individual; and these characteristics are in turn uniquely determined by the
constituents of the genes possessed by each individual. Ultimately, all human
behavior—hence all human society—is governed by a chain of determinants that
runs from the gene to the individual to the sum of the behaviors of all
individuals. The determinists would have it, then, that human nature is fixed by
our genes. The good society is either one in accord with a human nature to whose
fundamental characteristics of inequality and competitiveness the ideology
claims privileged access, or else it is an unattainable utopia because human
nature is in unbreakable contradiction with an arbitrary notion of the good
derived without reference to the facts of physical nature. The causes of social
phenomena are thus located in the biology of the individual actors in a social
scene, as when we are informed that the cause of the youth riots in many British
cities in 1981 must be sought in "a poverty of aspiration and of expectation
created by family, school, environment, and genetic inheritance."[c6]

What is more, biology, or "genetic inheritance," is always invoked as an
expression of inevitability: What is biological is given by nature and proved by
science. There can be no argument with biology, for it is unchangeable, a
position neatly exemplified in a television interview given by British Minister
for Social Services Patrick Jenkin in 1980 on working mothers:

#+BEGIN_QUOTE
Quite frankly, I don't think mothers have the same right to work as fathers. If
the Lord had intended us to have equal rights to go to work, he wouldn't have
created men and women. These are biological facts, young children do depend on
their mothers.
#+END_QUOTE

The use of the double legitimation of science and god is a bizarre but not
uncommon feature of New Right ideology: the claim to a hotline to the deepest
sources of authority about human nature.

The reductionist and biological determinist propositions that we shall examine
and criticize in the pages of this book are:

- Social phenomena are the sums of the behaviors of /individuals/.
- These behaviors can be treated as objects, that is, /reified/ into properties
  located in the brain of particular individuals.
- The reified properties can be measured on some sort of scale so that
  individuals can be ranked according to the amounts they possess.
- Population norms for the properties can be established: Deviations from the
  norm for any individual are abnormalities that may reflect medical problems
  for which the individual must be treated.
- The reified and medicalized properties are /caused/ by events in the brains of
  individuals—events that can be given anatomical localizations and are
  associated with changed quantities of particular biochemical substances.
- The changed concentrations of these biochemicals may be partitioned between
  genetic and environmental causes; hence the "degree of inheritance" or
  /heritability/ of differences may be measured.
- Treatment for abnormal amounts of the reified properties may be either to
  eliminate undesirable genes (eugenics, genetic engineering, etc.); or to find
  specific drugs ("magic bullets") to rectify the biochemical abnormalities or
  to excise or stimulate particular brain regions so as to eliminate the site of
  the undesirable behavior. Some lip service may be paid to supplementary
  environmental intervention, but the primary prescription is "biologized."

Working scientists may believe, or conduct experiments, based on one or more of
these propositions without feeling themselves to be full-fledged determinists in
the sense that we use the term: nonetheless, adherence to this general
analytical approach characterizes determinist methodology.

Biological determinism (/biologism/) has been a powerful mode of explaining the
observed inequalities of status, wealth, and power in contemporary industrial
capitalist societies, and of defining human "universals" of behavior as natural
characteristics of these societies. As such, it has been gratefully seized upon
as a political legitimator by the New Right, which finds its social nostrums so
neatly mirrored in nature: for if these inequalities are biologically
determined, they are therefore inevitable and immutable. What is more, attempts
to remedy them by social means, as in the prescriptions of liberals, reformists,
and revolutionaries, "go against nature." Racism, Britain's National Front tells
us, is a product of our "selfish genes."[c7] Nor are such political dicta
confined to the ideologues: Time and again, despite their professed belief that
their science is "above mere human politics" (to quote Oxford sociobiologist
Richard Dawkins),[c8] biological determinists deliver themselves of social and
political judgments. One example must suffice for now: Dawkins himself, in his
book /The Selfish Gene/, which is supposed to be a work on the genetic basis of
evolution and which is used as a textbook in American university courses on the
evolution of behavior, criticizes the "unnatural" welfare state where

#+BEGIN_QUOTE
we have abolished the family as a unit of economic self-sufficiency and
substituted the state. But the privilege of guaranteed support for children
should  not be abused. . . . Individual humans who have more children than they
are  capable of raising are probably too ignorant in most cases to be accused
of  conscious malevolent exploitation. Powerful institutions and leaders who 
deliberately encourage them to do so seem to me less free from suspicion.[c9]
#+END_QUOTE

The point is not merely that biological determinists are often somewhat naive
political and social philosophers. One of the issues with which we must come to
grips is that, despite its frequent claim to be neutral and objective, science
is not and cannot be above "mere" human politics. The complex interaction
between the evolution of scientific theory and the evolution of social order
means that very often the ways in which scientific research asks its questions
of the human and natural worlds it proposes to explain are deeply colored by
social, cultural, and political biases.[c10]

Our book has a two fold task: we are concerned first with an explanation of the
origins and social functions of biological determinism in general—the task of
the next two chapters—and second with a systematic examination and exposure of
the emptiness of its claims vis-à-vis the nature and limits of human society
with respect to equality, class, race, sex, and "mental disorder." We shall
illustrate this through a study of specific themes: IQ theory, the assumed basis
of differences in "ability" between sexes and races, the medicalization of
political protest, and, finally, the overall conceptual strategy of evolutionary
and adaptationist explanation offered by sociobiology in its modern forms. Above
all, this means an examination of the claims of biological determinism regarding
the "nature of human nature."

In examining these claims and in exposing the pseudoscientific, ideological, and
often just simply methodologically inadequate findings of biological
determinism, it is important for us, and for our readers, to be clear about the
position we ourselves take.

Critics of biological determinism have frequently drawn attention to the
ideological role played by apparently scientific conclusions about the human
condition that seem to flow from biological determinism. That, despite their
pretensions, biological determinists are engaged in making political and moral
statements about human society, and that their writings are seized upon as
ideological legitimators, says nothing, in itself, about the scientific merits
of their claims.[c11] Critics of biological determinism are often accused of
merely disliking its political conclusions. We have no hesitation in agreeing
that we do dislike these conclusions; we believe that it is possible to create a
better society than the one we live in at present; that inequalities of wealth,
power, and status are not "natural" but socially imposed obstructions to the
building of a society in which the creative potential of all its citizens is
employed for the benefit of all.

We view the links between values and knowledge as an integral part of doing
science in this society at all, whereas determinists tend to deny that such
links exist—or claim that if they do exist they are exceptional pathologies to
be eliminated. To us such an assertion of the separation of fact from value, of
practice from theory, "science" from "society" is itself part of the
fragmentation of knowledge that reductionist thinking sustains and which has
been part of the mythology of the last century of "scientific advance" (see
Chapters 3 and 4). However, the least of our tasks here is that of criticizing
the social implications of biological determinism, as if the broad claims of
biological determinism could be upheld. Rather, our major goal is to show that
the world is not to be understood as biological determinism would have it be,
and that, as a way of explaining the world, biological determinism is
fundamentally flawed.

Note that we say "the world," for another misconception is that the criticism of
'biological determinism applies only to its conclusions about human societies,
while what it says about nonhuman animals is more or less valid. Such a view is
often expressed—for instance about E. O. Wilson's book /Sociobiology: The New
Synthesis/,[c12] which we discuss at length in Chapter 9. Its liberal critics
claim that the problem with /Sociobiology/ lies only in the first and last
chapters, where the author discusses human sociobiology; what's in between is
true. Not so, in our view: what biological determinism has to say about human
society is more wrong than what it says about other aspects of biology because
its simplifications and misstatements are the more gross. But this is not
because it has developed a theory applicable only to nonhuman animals; the
method and theory are fundamentally flawed whether applied to the United States
or Britain today, or to a population of savanna-dwelling baboons or Siamese
fighting fish.

There is no mystical and unbridgeable gulf between the forces that shape human
society and those that shape the societies of other organisms; biology is indeed
relevant to the human condition, although the form and extent of its relevance
is far less obvious than the pretensions of biological determinism imply. The
antithesis often presented as an opposition to biological determinism is that
biology stops at birth, and from then on culture supervenes. This antithesis is
a type of cultural determinism we would reject, for the cultural determinists
identify narrow (and exclusive) causal chains in society which are in their own
way reductionist as well. Humanity cannot be cut adrift from its own biology,
but neither is it enchained by it.

Indeed, one may see in some of the appeal of biological determinist and New
Right writing a reassertion of the "obvious" against the very denial of biology
that has characterized some of the utopian writings and hopes of the
revolutionary movements of the past decade. The post-1968 New Left in Britain
and the United States has shown a tendency to see human nature as almost
infinitely plastic, to deny biology and acknowledge only social construction.
The helplessness of childhood, the existential pain of madness, the frailties of
old age were all transmuted to mere labels ref;acting disparities in power.[c13]
But this denial of biology is so contrary to actual lived experience that it has
rendered people the more ideologically vulnerable to the "common-sense" appeal
of reemerging biological determinism. Indeed, we argue in Chapter 3 that such
cultural determinism can be as oppressive in obfuscating real knowledge about
the complexity of the world we live in as is biological determinism. We do not
offer in this book a blueprint or a catalogue of certainties; our task, as we
see it, is to point the way toward an integrated understanding of the
relationship between the biological and the social.

We describe such an understanding as dialectical, in contrast to reductionist.
Reductionist explanation attempts to derive the properties of wholes from
intrinsic properties of parts, properties that exist apart from and before the
parts are assembled into complex structures. It is characteristic of
reductionism that it assigns relative weights to different partial causes and
attempts to assess the importance of each cause by holding all others constant
while varying a single factor. Dialectical explanations, on the contrary, do not
abstract properties of parts in isolation from their associations in wholes but
see the properties of parts as arising out of their associations. That is,
according to the dialectical view, the properties of parts and wholes
codetermine each other. The properties of individual human beings do not exist
in isolation but arise as a consequence of social life, yet the nature of that
social life is a consequence of our being human and not, say, plants. It
follows, then, that dialectical explanation contrasts with cultural or dualistic
modes of explanation that separate the world into different types of
phenomena—culture and biology, mind and body—which are to be explained in quite
different and nonoverlapping ways.

Dialectical explanations attempt to provide a coherent, unitary, but
nonreductionist account of the material universe. For dialectics the universe is
unitary but always in change, the phenomena we can see at any instant are parts
of processes, processes with histories and futures whose paths are not uniquely
determined by their constituent units. Wholes are composed of units whose
propervies may be described, but the interaction of these units in the
construction of the wholes generates complexities that result in products
qualitatively different from the component parts. Think, for example, of the
baking of a cake: the taste of the product is the result of a complex
interaction of components—such as butter, sugar, and flour—exposed for various
periods to elevated temperatures; it is not dissociable into such-or-such a
percent of flour, such-or-such of butter, etc., although each and every
component (and their development over time at a raised temperature) has its
contribution to make to the final product. In a world in which such complex
developmental interactions are always occurring, history becomes of paramount
importance. Where and how an organism is now is not merely dependent upon its
composition at this time but upon a past that imposes contingencies on the
present and future interaction of its components.

Such a world view abolishes the antitheses of reductionism and dualism; of
nature/nurture or of heredity/environment; of a world in stasis whose components
interact in fixed and limited ways, indeed in which change is possible only
along fixed and previously definable pathways. In the chapters that follow, the
explication of this position will appear in the course of the development of our
opposition to biological determinism—in our analysis, for instance, of the
relationship of genotype and phenotype (in Chapter 5), and of mind and brain.

Let us take just one example here, that of the relationship of the organism to
its environment. Biological determinism sees organisms, human or nonhuman, as
adapted by evolutionary processes to their environment, that is, fitted by the
processes of genetic reshuffling, mutation, and natural selection to maximize
their reproductive success in the environment in which they are born and
develop; further, it sees the undoubted plasticity of organisms—especially
humans—as they develop as a series of modifications imposed upon an essentially
passive, recipient object by the buffeting of "the environment" to which it is
exposed and to which it must adapt or perish. Against this we counterpose a view
not of organism and environment insulated from one another or unidirectionallv
affected, but of a constant and active interpenetration of the organism with its
environment. Organisms do not merely receive a given environment but actively
seek alternatives or change what they find.

Put a drop of sugar solution onto a dish containing bacteria and they will
actively move toward the sugar till they arrive at the site of optimum
concentration, thus changing a low-sugar for a high-sugar environment. They will
then actively work on the sugar molecules, changing them into other
constituents, some of which they absorb, others of which they put out into the
environment, thereby modifying it, often in such a way that it becomes, for
example, more acid. When this happens, the bacteria move away from the highly
acid region to regions of lower acidity. Here, in miniature, we see the case of
an organism "choosing" a preferred environment, actively working on it and so
changing it, and then "choosing™ an alternative.

Or consider a bird building a nest. Straw is not part of the bird's environment
unless it actively seeks it out so as to construct its nest: in doing so it
changes its environment, and indeed the environment of other organisms as well.
The "environment" itself is under constant modification by the activity of all
the organisms within it. And to any organism, all others form part of its
"environment"—predators, prey, and those that merely change the landscape it
resides in.[c14]

Even for nonhumans, then, the interaction of organism and environment is far
from the simplistic models offered by biological determinism. And this is much
more the case for our own species. All organisms bequeath to their successors
when they die a slightly changed environment; humans above all are constantly
and profoundly making over their environment in such a way that each generation
is presented with quite novel sets of problems to explain and choices to make;
we make our own history, though in circumstances not of our own choosing.

It is precisely because of this that there are such profound difficulties with
the concept of "human nature." To the biological determinists the old credo "You
can't change human nature" is the alpha and omega of the explanation of the
human condition. We are not concerned to deny that there is a "human nature,"
which is simultaneously biologically and socially constructed, though we find it
an extraordinarily elusive concept to pin down; in our discussion on
sociobiology in Chapter 9 we analyze the best list of human "universals" that
protagonists of sociobiology have been able to present.

Of course there are human universals that are in no sense trivial: humans are
bipedal; they have hands that seem to be unique among animals in their capacity
for sensitive manipulation and construction of objects; they are capable of
speech. The fact that human adults are almost all greater than one meter and
less than two meters in height has a profound effect on how they perceive and
interact with their environment. If humans were the size of ants, we would have
an entirely different set of relations with the objects that constitute our
world: similarly, if we had eyes that were sensitive, like those of some
insects, to ultraviolet wavelengths, or if, like some fishes, we had organs
sensitive to electrical fields, the range of our interactions with each other
and with other organisms would doubtless be very different. If we had wings,
like birds, we would construct a very different world.

In this sense, the environments that human organisms seek and those they create
are in accord with their nature. But just what does this mean? The human
chromosomes may not contain the genes that, in the development of the phenotype,
are associated with ultraviolet vision, or sensitivity to electrical fields, or
wings. Indeed, in the last case there are structural reasons quite independent
of genetic ones why organisms of the weight of humans cannot develop wings large
or powerful enough to enable them to fly. And indeed, for a considerable
proportion of human history it has gone against human nature to be able to do
any of these things. However, as is apparent to all of us, in our present
society we can do all of these things: see in the ultraviolet; detect electrical
fields: fly by machine, wind, or even pedal power. It is, clearly, "in" human
nature to so modify our environment that all these activities come well within
our range (and hence within the range of our genotype).

Even where the acts we perform on our environment appear to be biologically
equivalent, they are not necessarily socially equivalent. Hnger is hunger (the
anthropologist Lévi-Strauss has made this given the basis of a complex human
structural typology); yet hunger satisfied by eating raw meat with hands and
fingers is quite different from that satisfied by eating cooked meat with a
knife and fork. All humans are born, most procreate, all die: yet the social
meanings invested in any of these acts vary profoundly from culture to culture
and from context to context within a culture.

This is why about the only sensible thing to say about human nature is that it
is "in" that nature to construct its own history. The consequence of the
construction of that history is that one generation's limits to the nature of
human nature become irrelevant to the next. Take the concept of intelligence. To
an earlier generation, the capacity to perform complex long multiplication or
division was laboriously acquired by children fortunate enough to go to school.
Many never achieved it; they grew up lacking, for whatever reason, the ability
to perform the calculations. Today, with no more than minimal training, such
calculating power and considerably more are at the disposal of any five-year-old
who can manipulate the buttons on a calculator. The products of one human
generation's intelligence and creativity have been placed at the disposal of a
subsequent generation, and the horizons of human achievement have been thereby
extended. The intelligence of a schoolchild today, in any reasonable
understanding of the term, is quite different from and in many ways much greater
than that of his or her Victorian counterpart, or that of a feudal lord or of a
Greek slaveowner. Its measure is itself historically contingent.

Because it is in human nature so to construct our own history, and because the
construction of our history is made as much with ideas and words as with
artifacts, the advocacy of biological determinist ideas, and the argument
against them, are themselves part of that history. Alfred Binet, the founder of
IQ testing, once protested against "the brutal pessimism" that regards a child's
IQ score as a fixed measure of his or her ability, rightly seeing that to regard
the child as thus fixed was to help ensure that he or she remained so.
Biological determinist ideas are part of the attempt to preserve the
inequalities of our society and to shape human nature in their own image. The
exposure of the fallacies and political content of those ideas is part of the
struggle to eliminate those inequalities and to transform our society. In that
struggle we transform our own nature.

# Chapter over. Continue from page 29 of the DJVU.

# Footnotes

[fn:1] We should make it clear that we use the term ideology here and throughout
this book with a precise meaning. Ideologies are the ruling ideas of a
particular society at a particular time. They are ideas that express the
"naturalness" of any existing social order and help maintain it.

The ideas of the ruling class are in every epoch the ruling ideas: i.e. the
class which is the ruling material force of society is at the same time its
ruling intellectual force. The class which has the means of material production
at its disposal has control at the same time over the means of mental
production, so that thereby, generally speaking, the ideas of those who lack the
means of mental production are subject to it. The ruling ideas are nothing more
than the ideal expression of the dominant material relationships.[c2]
